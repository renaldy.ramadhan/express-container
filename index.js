const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.json({ status: 'The server is up and running' })
})
app.get('/deploy', (req, res) => {
  res.json({ status: 'Node Js app containerize using container' })
})

app.listen(3000, () => {
  console.log('Server is running at port 3000')
})
